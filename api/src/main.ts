import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import 'reflect-metadata'; // For TypeORM
import { DataSource } from 'typeorm';
import { User } from './entities/User';
import { Vendor } from './entities/Vendor';
import { VendorUpdate } from './entities/VendorUpdate';

const dataSource = new DataSource({
  type: 'postgres',
  host: 'database', // Docker container name for the db
  port: 5432,
  username: 'steven',
  password: 'gabagool',
  database: 'market',
  entities: [User, Vendor, VendorUpdate],
  synchronize: true,
  logging: false,
});

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  await app.listen(8080);
}
dataSource
  .initialize()
  .then(() => {
    bootstrap();
  })
  .catch((error) => console.error(error));
