import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  Column,
} from 'typeorm';
import { Vendor } from './Vendor';

@Entity()
export class VendorUpdate extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne((type) => Vendor, (vendor) => vendor.id)
  @JoinColumn({ name: 'vendor_id' })
  vendor: Vendor;

  @Column('timestamptz', {
    default: () => 'CURRENT_TIMESTAMP',
  })
  created_at: Date;

  @Column({ nullable: false })
  content: string;
}
