import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  Unique,
  OneToMany,
} from 'typeorm';
import { VendorUpdate } from './VendorUpdate';

@Entity()
@Unique(['slug'])
export class Vendor extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  slug: string;

  @Column()
  name: string;

  @Column({ nullable: false, default: true })
  coming_to_market: boolean;

  @Column({ nullable: true })
  icon: string;

  @Column({ nullable: true })
  biography: string;

  @Column({ nullable: true })
  products: string;

  @OneToMany(() => VendorUpdate, (vendorUpdate) => vendorUpdate.vendor)
  updates: VendorUpdate[];
}
