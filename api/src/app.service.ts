import { Injectable } from '@nestjs/common';
import { Vendor } from './entities/Vendor';
import { VendorUpdate } from './entities/VendorUpdate';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
  async getVendors() {
    let vendors = await Vendor.find({ relations: ['updates'] });
    return vendors;
  }
  async getVendor(slug: string) {
    let vendor = await Vendor.findOneBy({ slug });
    let updates = [];
    if (vendor) {
      updates = await VendorUpdate.findBy({ vendor: { id: vendor.id } });
      vendor.updates = updates;
    }
    return vendor;
  }
  async postUpdate(id: number, content: string) {
    const vendor = await Vendor.findOneBy({ id });
    if (vendor) {
      let update = new VendorUpdate();
      update.vendor = vendor;
      update.content = content;
      return update.save();
    } else {
      return null;
    }
  }
  async editUpdate(vendorId: number, updateId: number, content: string) {
    let update = await VendorUpdate.findOneBy({
      vendor: { id: vendorId },
      id: updateId,
    });
    if (update) {
      update.content = content;
      return update.save();
    } else {
      return null;
    }
  }
}
