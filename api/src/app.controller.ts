import { Controller, Get, Post, Param, Body, Put } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('vendors')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getVendors() {
    return this.appService.getVendors();
  }

  @Get(':slug')
  getVendor(@Param('slug') slug: string) {
    return this.appService.getVendor(slug);
  }

  @Put(':id/update')
  createVendorUpdate(@Param('id') id: string, @Body() body: any) {
    return this.appService.postUpdate(Number(id), body.update);
  }

  @Post(':vendorId/update/:updateId')
  editVendorUpdate(
    @Param('vendorId') vendorId: string,
    @Param('updateId') updateId: string,
    @Body() body: any,
  ) {
    return this.appService.editUpdate(
      Number(vendorId),
      Number(updateId),
      body.update,
    );
  }
}
