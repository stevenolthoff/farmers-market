import axios from "axios";

export async function getVendors() {
    try {
        const response = await axios.get("http://localhost:8080/vendors");
        return response.data;
    } catch (error) {
        console.error(error);
        return [];
    }
}

export async function getVendor(slug) {
    try {
        const response = await axios.get(
            `http://localhost:8080/vendors/${slug}`
        );
        return response.data;
    } catch (error) {
        console.error(error);
        return null;
    }
}

export async function postUpdate(id, update) {
    try {
        const response = await axios.put(
            `http://localhost:8080/vendors/${id}/update`,
            {
                update,
            }
        );
        return response.data;
    } catch (error) {
        console.error(error);
        return null;
    }
}
