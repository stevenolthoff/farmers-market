import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';
import './App.css';
import Home from './views/Home';
import Vendor from './views/Vendor';

function App() {
  return (
    <Router>
      <div className='App'>
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="/vendors/:slug" element={<Vendor />}></Route>
        </Routes>
       </div>
    </Router>
  );
}

export default App;
