import { useEffect, useState } from "react";
import { useParams } from "react-router";
import { getVendor, postUpdate } from "../api";
import { getNewUpdateCount, isNewUpdate } from "../utilities";
import ProductList from "./components/ProductList";
import moment from "moment";
import parse from "html-react-parser";

export default function Vendor() {
    const { slug } = useParams();
    const [vendor, setVendor] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [newUpdateText, setNewUpdateText] = useState(null);
    const [postUpdateButtonEnabled, setPostUpdateButtonEnabled] =
        useState(false);

    useEffect(() => {
        async function getVendorFromApi() {
            const newVendor = await getVendor(slug);
            setVendor(newVendor);
            setIsLoading(false);
        }
        getVendorFromApi();
    }, [slug]);

    function renderUpdates() {
        const updates = vendor.updates.sort(
            (a, b) => new Date(b.created_at) - new Date(a.created_at)
        );
        return updates.map((update, i) => {
            const isNew = isNewUpdate(update);
            let newUpdateBadgeClasses = "";
            if (isNew) {
                newUpdateBadgeClasses = "bg-red-500 w-3 h-3 rounded-full";
            } else {
                newUpdateBadgeClasses = "invisible w-3 h-3";
            }
            const newUpdateBadge = (
                <div className={newUpdateBadgeClasses}></div>
            );

            const currentUpdate = (
                <div className="py-4" key={`update-${i}`}>
                    <div className="flex gap-2">
                        {newUpdateBadge}
                        <p className="text-slate-500">
                            {moment(update.created_at).format("dddd, MMM D, Y")}
                        </p>
                    </div>
                    <p className="pl-5">{parse(update.content)}</p>
                    {/* <button className="mr-2 ml-1 mt-4 h-8 px-4 text-sm rounded-md text-slate-400 hover:text-slate-500 hover:bg-slate-300 active:bg-slate-400 active:text-slate-600">
                        Edit
                    </button>
                    <button className="ml-1 mt-4 h-8 px-4 text-sm rounded-md text-slate-400 hover:text-slate-500 hover:bg-slate-300 active:bg-slate-400 active:text-slate-600">
                        Delete
                    </button> */}
                </div>
            );
            if (i < vendor.updates.length - 1) {
                return (
                    <div>
                        {currentUpdate}
                        <hr />
                    </div>
                );
            } else {
                return currentUpdate;
            }
        });
    }

    function renderPostUpdateButton() {
        let classes =
            "text-white uppercase font-semibold p-4 rounded-md max-h-14";
        if (postUpdateButtonEnabled) {
            classes += " bg-blue-500 hover:bg-blue-600 active:bg-blue-700";
        } else {
            classes += " bg-slate-300 hover:cursor-not-allowed";
        }
        return (
            <button className={classes} onClick={handlePostUpdate}>
                Post Update
            </button>
        );
    }

    async function handlePostUpdate() {
        if (!newUpdateText) return;
        else {
            const newUpdate = await postUpdate(vendor.id, newUpdateText);
            setNewUpdateText("");
            setPostUpdateButtonEnabled(false);
            let updates = vendor.updates.slice();
            updates.push({
                id: newUpdate.id,
                created_at: newUpdate.created_at,
                content: newUpdate.content,
            });
            setVendor({ ...vendor, updates });
        }
    }

    function handleNewUpdateTextChange(event) {
        const newUpdate = event.target.value;
        setNewUpdateText(newUpdate);
        setPostUpdateButtonEnabled(!!newUpdate && newUpdate.length > 0);
    }

    if (isLoading) {
        return <div>Loading…</div>;
    } else {
        const numNewUpdates = getNewUpdateCount(vendor.updates);
        let numNewUpdateString = "New Update";
        if (numNewUpdates === 0 || numNewUpdates > 1) {
            numNewUpdateString = "New Updates";
        }
        return (
            <div className="sm:flex sm:space-x-8 p-4 max-h-screen">
                <div>
                    <img src={vendor.icon} className="top-0 sm:max-w-lg" />
                </div>
                <div className="text-left flex flex-col gap-4 overflow-y-scroll">
                    <div>
                        <p className="text-4xl text font-semibold">
                            {vendor.name}
                        </p>
                        <p className="uppercase font-medium text-slate-500">
                            Pastured Poultry, Organic, Non-GMO Feed
                        </p>
                        <div className="pt-4">
                            <ProductList products={vendor.products} />
                        </div>
                    </div>
                    <hr />
                    <p className="text-slate-500">{vendor.biography}</p>
                    <hr />
                    <div className="flex space-x-2 place-items-center">
                        <div className="flex flex-col gap-2 flex-grow">
                            <div className="flex gap-2 place-items-center">
                                <div
                                    className={`
                                        ${
                                            numNewUpdates > 0
                                                ? "bg-red-500"
                                                : "bg-slate-300"
                                        }
                                    flex place-items-center justify-center w-8 h-4 rounded-md text-white font-semibold text-center`}
                                >
                                    <p>{numNewUpdates}</p>
                                </div>
                                <p
                                    className={`${
                                        numNewUpdates === 0
                                            ? "text-slate-400"
                                            : ""
                                    } uppercase font-medium`}
                                >
                                    {numNewUpdateString}
                                </p>
                            </div>
                            <div className="flex gap-4 justify-between">
                                <textarea
                                    type="text"
                                    placeholder="What's New?"
                                    className="border rounded-md p-4 grow h-14"
                                    value={newUpdateText}
                                    onChange={handleNewUpdateTextChange}
                                ></textarea>
                                {renderPostUpdateButton()}
                            </div>
                            <div>{renderUpdates()}</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
