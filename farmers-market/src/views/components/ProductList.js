import render from "dom-serializer";

export default function ProductList(props) {
    const productListItems = props.products.split(",").map((product, i) => (
        <div
            key={`product-${i}`}
            className="uppercase bg-blue-500 text-white font-semibold rounded-md px-3"
        >
            {product}
        </div>
    ));
    return (
        <div className="flex flex-wrap gap-x-3 gap-y-2">{productListItems}</div>
    );
}
