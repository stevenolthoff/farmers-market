import ProductList from "./ProductList";
import { getNewUpdateCount, isNewUpdate } from "../../utilities";
import { Link } from "react-router-dom";

export default function VendorListItem(props) {
    function renderNewUpdateBadge() {
        const numNewUpdates = getNewUpdateCount(props.vendor.updates);
        if (numNewUpdates > 0) {
            return (
                <div className="bg-red-500 w-4 h-4 rounded-full translate-x-0 translate-y-5"></div>
            );
        } else {
            return null;
        }
    }

    function renderPresentStatus() {
        if (props.vendor.coming_to_market) {
            return (
                <p className="text-green-500 font-medium">
                    &#9989; Will Be Present
                </p>
            );
        } else {
            return (
                <p className="text-slate-400 font-medium">
                    Will Not Be Present
                </p>
            );
        }
    }

    return (
        <Link exact="true" to={`/vendors/${props.vendor.slug}`}>
            <div className="rounded-xl border-solid border-slate-300 border p-4 hover:bg-slate-100 hover:border-blue-300">
                <div className="flex">
                    <div>
                        {renderNewUpdateBadge()}
                        <img
                            src={props.vendor.icon}
                            className="h-24 pr-4"
                        ></img>
                    </div>
                    <div className="text-left">
                        <Link exact="true" to={`/vendors/${props.vendor.slug}`}>
                            <p className="text-lg font-semibold">
                                {props.vendor.name}
                            </p>
                        </Link>
                        {renderPresentStatus()}
                    </div>
                </div>
                <ProductList products={props.vendor.products} />
            </div>
        </Link>
    );
}
