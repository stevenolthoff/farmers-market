import { useEffect, useState } from "react";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import { getVendors } from "../api";
import VendorListItem from "./components/VendorListItem";

export default function Home() {
    const [isLoading, setIsLoading] = useState(true);
    const [vendors, setVendors] = useState([]);

    useEffect(() => {
        async function getVendorsFromApi() {
            const newVendors = await getVendors();
            setVendors(newVendors);
            setIsLoading(false);
        }
        getVendorsFromApi();
    }, []);

    if (isLoading) {
        return <div>Loading…</div>;
    } else {
        const vendorElements = vendors.map((vendor) => (
            <VendorListItem
                className=""
                key={`vendor-${vendor.id}`}
                vendor={vendor}
            />
        ));
        return (
            <div>
                <p className="text-4xl font-semibold text-left p-4 pt-8">
                    Market Vendors
                </p>
                <div className="grid gap-4 p-4">{vendorElements}</div>
            </div>
        );
    }
}
