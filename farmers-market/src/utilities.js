import moment from "moment";

export function isNewUpdate(update) {
    const previousMarketDate = moment().day(-3); // Previous Thursday
    return moment(update.created_at).isAfter(previousMarketDate);
}

export function getNewUpdateCount(updates) {
    return updates.filter((update) => isNewUpdate(update)).length;
}
